<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Setting;
use App\Entity\Activity;
use App\Helper\AsciiReplace;
use \DomDocument;
use \DOMXpath;
use Doctrine\ORM\EntityManagerInterface;
use JiraRestApi\Configuration\DotEnvConfiguration;
use JiraRestApi\JiraClient;
use JiraRestApi\JiraException;

class JiraActivityService
{

	const JIRA_ENDPOINT = '/activity?os_authType=basic';

	const JIRA_DBKEY = 'jira';

	private $dom;

	private $xpath;

	private $settings;

	protected $em;

	private $known_users;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}

	private function getDom()
	{

		$this->dom = new DomDocument;
		$this->dom->validateOnParse = false;
		$this->dom->strictErrorChecking = false;
		$this->dom->recover = true;

		try {
			@$this->dom->loadHTML('<?xml encoding="utf-8" ?>' . $this->response);
		} catch (Exception $e) {
			throw new Expection('Can\'t create DOM from string' . $e->getMessage());
		}

		return $this->dom;
	}

	private function getXpath()
	{
		$this->xpath = new DOMXpath($this->getDom());
	}


	/**
	 * get all activity feed.
	 */
	public function getActivityFeed()
	{
		$api_url = self::JIRA_ENDPOINT;

		$settings = $this->getSettings();

		if (isset($settings[self::JIRA_DBKEY])) {
			$api_url .= '&maxResults=100&relativeLinks=true&_=' . ((integer)$settings['jira'] - 4604800);
		}

		$client = new JiraClient;
		$client->setAPIUri($api_url);

		// TODO: Check if data is valid before further processing
		$this->response = $client->exec('', null);

		$this->setJiraLastTime();

	}

	public function setJiraLastTime()
	{

		$setting = $this->em->getRepository(Setting::class)->find(self::JIRA_DBKEY);
		$setting->setValue((string)time());

		$this->em->persist($setting);

	}

	public function getSettings()
	{
		$result = $this->em
			->getRepository(Setting::class)
			->findAll();

		$data = [];
		foreach ($result as $row) {
			$data[$row->getId()] = $row->getValue();
		}

		$this->settings = $data;

		return $this->settings;
	}

	/*
	 * Walk xml tree
	 */
	public function getActivityArray()
	{
		$entries = $this->xpath->query('//entry');
		foreach ($entries as $entry) {

			$activity = new Activity();

			$name = $this->xpath->query('.//name', $entry)[0];
			if (!empty($name) && is_object($name)) {
				$activity->setName($name->textContent);
			}

			$email = $this->xpath->query('.//email', $entry)[0];
			if (!empty($email) && is_object($email)) {
				$activity->setEmail($email->textContent);
			}

			$published = $this->xpath->query('.//published', $entry)[0];
			if (!empty($published) && is_object($published)) {
				$activity->setPublished($published->textContent);
			}

			$dateString = $this->xpath->query('.//updated', $entry)[0];
			if (!empty($dateString) && is_object($dateString)) {
				$date = date_create_from_format('Y-m-d\TH:i:s',
					substr($dateString->textContent, 0, 19));
				$activity->setUpdated($date);
			}

			$data[] = $activity;
		}

		return $data;
	}

	private function createUserFromActivity($activity)
	{
		$newuser = new User();
		$newuser->setName($activity->getName());
		$newuser->setEmail($activity->getEmail());

		// TODO: Check if Updated is bigger than Published
		$newuser->setActivityJira($activity->getUpdated());

		$this->em->persist($newuser);
		$this->em->flush();

		// Add user to known_users
		$this->known_users[] = $newuser;

		return true;
	}

	private function loadAllUsers()
	{
		// INFO
		// TODO: MaxResults will cause duplication of users if we get over the limit
		$this->known_users = $this->em
			->getRepository(User::class)
			->createQueryBuilder('e')
			->select('e')
			->orderBy('e.activity_jira', 'ASC')
			->setMaxResults(9000)
			->getQuery()
			->execute();

	}

	public function isKnownUser($user)
	{

		foreach ($this->known_users as $known_user) {
			// Try to match users by email
			if (trim(mb_strtolower($user->getEmail())) == trim(mb_strtolower($known_user->getEmail()))) {
				return $known_user;
				break;
			}

			// Try to match users by name
			if (trim(mb_strtolower(AsciiReplace::replace($user->getName()))) == trim(mb_strtolower(AsciiReplace::replace($known_user->getName())))) {
				return $known_user;
				break;
			}
		}

		return false;
	}

	public function loadJiraActivity()
	{

		// Load all users from db
		$this->loadAllUsers();

		try {

			// Load xml
			$this->getActivityFeed();
			$this->getXpath();
			$data = $this->getActivityArray();

			// Process data from oldest to newest
			$data = array_reverse($data);

			// Do we know this user?
			foreach ($data as $activity) {

				if ($known_user = $this->isKnownUser($activity)) {
					// yes
					// update user time
					$known_user->setActivityJira($activity->getUpdated());
					$this->em->persist($known_user);
					$this->em->flush();
				} else {

					// no
					// create user
					$this->createUserFromActivity($activity);
				}

			}

		} catch (JiraException $e) {
			print("Error Occured! " . $e->getMessage());
		}

	}


}

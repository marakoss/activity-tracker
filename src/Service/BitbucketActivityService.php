<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Setting;
use App\Entity\Activity;
use App\Helper\AsciiReplace;
use Bitbucket\API\Repositories;
use Bitbucket\API\Repositories\Commits;
use Bitbucket\API\Authentication\Basic as BasicAuth;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Dotenv\Dotenv;

class BitbucketActivityService
{
	const BITBUCKET_USER = 'usertech';

	const BITBUCKET_DBKEY = 'bitbucket';

	private $settings;

	protected $em;

	private $known_users;

	private $bitbucketUser;

	private $bitbucketPass;

	private $page;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;

		$this->bitbucketUser = getenv('BITBUCKET_USER');
		$this->bitbucketPass = getenv('BITBUCKET_PASS');

		$this->getSettings();
	}


	public function setBitbucketPage($page)
	{

		$setting = $this->em->getRepository(Setting::class)->find(self::BITBUCKET_DBKEY);

		$setting->setValue(strval($page));

		$this->em->persist($setting);
		$this->em->flush();

	}


	/*
	 * Load settings
	 * TODO: Move to separate service?
	 */
	public function getSettings()
	{
		$result = $this->em
			->getRepository(Setting::class)
			->findAll();

		$data = [];
		foreach ($result as $row) {
			$data[$row->getId()] = $row->getValue();
		}

		$this->settings = $data;

		return $this->settings;
	}


	private function createUserFromActivity($activity)
	{
		$newuser = new User();
		$newuser->setName($activity->getName());
		$newuser->setEmail($activity->getEmail());
		$newuser->setActivityBitbucketCommit($activity->getCommit());

		// TODO: Check if Updated is bigger than Published
		$newuser->setActivityBitbucket($activity->getUpdated());

		$this->em->persist($newuser);
		$this->em->flush();

		// Add user to known_users
		$this->known_users[] = $newuser;

		return true;
	}

	private function loadAllUsers()
	{

		// INFO
		// TODO: MaxResults will cause duplication of users if we get over the limit
		$this->known_users = $this->em
			->getRepository(User::class)
			->createQueryBuilder('e')
			->select('e')
			->orderBy('e.activity_bitbucket', 'ASC')
			->setMaxResults(9000)
			->getQuery()
			->execute();

	}

	public function isKnownUser($user)
	{

		foreach ($this->known_users as $known_user) {
			// Try to match users by email
			if (trim(mb_strtolower($user->getEmail())) == trim(mb_strtolower($known_user->getEmail()))) {
				return $known_user;
				break;
			}

			// Try to match users by name
			if (trim(mb_strtolower(AsciiReplace::replace($user->getName()))) == trim(mb_strtolower(AsciiReplace::replace($known_user->getName())))) {
				return $known_user;
				break;
			}
		}

		return false;
	}

	public function getPage()
	{

		if (isset($this->settings[self::BITBUCKET_DBKEY])) {

			$this->page = $this->settings[self::BITBUCKET_DBKEY];

			return $this->page;
		}

		return 1; // start

	}

	public function setNextPage($page, $size, $len)
	{

		if ($len == 0) {
			throw new \Exception('Cannot divide by zero.');
		}

		$max = ceil($size / $len);

		if ($page >= $max) {
			$this->setBitbucketPage('1');

		} else{
			$this->setBitbucketPage($page + 1);
		}

	}

	public function getActivity()
	{
		$data = [];

		$repos = new Repositories();
		$repos->setCredentials(new BasicAuth($this->bitbucketUser, $this->bitbucketPass));

		if ($this->getPage() < 2) {
			$content = $repos->all(self::BITBUCKET_USER);
		} else {
			$content = $repos->all(self::BITBUCKET_USER, ['page' => $this->getPage()]);
		}
		$repos_json = json_decode($content->getContent());


		if (property_exists($repos_json, 'page')) {
			$this->setNextPage($repos_json->page, $repos_json->size, $repos_json->pagelen);
		} else {
			$this->setNextPage(1, 1, 1);
		}

		foreach ($repos_json->values as $repository) {

			$commits = new Commits();
			$commits->setCredentials(new BasicAuth($this->bitbucketUser, $this->bitbucketPass));
			$commits_json = json_decode($commits->all(self::BITBUCKET_USER,
				$repository->slug)->getContent());

			foreach ($commits_json->values as $item) {

				$activity = new Activity();

				// Check if the commit was made by registered user
				if (!property_exists($item->author, 'user')) {
					continue;
				}

				// Name
				$activity->setName($item->author->user->display_name);

				// Email
				preg_match('/\<(.*)\>/', $item->author->raw, $matches);
				$activity->setEmail($matches[1]);

				// Date
				$date = date_create_from_format('Y-m-d\TH:i:sP', $item->date);
				$activity->setUpdated($date);

				// Commit
				$commit = @$item->parents[0]->links->self->href;
				$activity->setCommit($commit);

				$data[] = $activity;
			}

		}

		return $data;
	}

	public function loadBitbucketActivity()
	{


		$data = $this->getActivity();

		// Load all users from db
		$this->loadAllUsers();

		// Do we know this user?
		foreach ($data as $activity) {

			if ($known_user = $this->isKnownUser($activity)) {
				// yes
				// update user time
				// if the activity time is newer
				if ($known_user->getActivityBitbucket() < $activity->getUpdated()) {
					$known_user->setActivityBitbucket($activity->getUpdated()); // update time
					$known_user->setActivityBitbucketCommit($activity->getCommit()); // update commit
					$this->em->persist($known_user);
					$this->em->flush();
				}

			} else {

				// no
				// create user
				$this->createUserFromActivity($activity);
			}
		}

	}


}

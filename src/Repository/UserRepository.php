<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class UserRepository extends EntityRepository
{
	public function OrderedDate()
	{
		$rsm = new ResultSetMappingBuilder($this->getEntityManager());
		$rsm->addRootEntityFromClassMetadata('App:User', 'p');
		$result = $this->getEntityManager()
			//->createQuery(
			//	'SELECT p, GREATEST(p.activity_bitbucket, p.activity_jira) as great FROM App:User p ORDER BY great ASC, p.activity_bitbucket ASC, p.activity_jira ASC'
			//)
			->createNativeQuery('SELECT *, LEAST(p.activity_bitbucket, p.activity_jira) as great FROM user p ORDER BY great ASC, p.activity_bitbucket ASC, p.activity_jira ASC', $rsm)
			->getResult();
		//var_dump($result);
		//die();
		return $result;
	}
}
<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255, )
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $email;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $activity_jira;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $activity_bitbucket;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $activity_bitbucket_commit;

	private $last_activity;

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($val)
	{
		$this->name = $val;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($val)
	{
		$this->email = $val;
	}

	public function getActivityJira()
	{
		return $this->activity_jira;
	}

	public function setActivityJira($val)
	{
		$this->activity_jira = $val;

	}

	public function getActivityBitbucket()
	{
		return $this->activity_bitbucket;
	}

	public function setActivityBitbucket($val)
	{
		$this->activity_bitbucket = $val;

	}

	public function getLastActivity()
	{
		return $this->last_activity;
	}

	public function setLastActivity($val)
	{
		$this->last_activity = $val;
	}

	public function getActivityBitbucketCommit()
	{
		return $this->activity_bitbucket_commit;
	}

	public function setActivityBitbucketCommit($val)
	{
		$this->activity_bitbucket_commit = $val;
	}
}
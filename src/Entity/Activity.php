<?php

namespace App\Entity;

class Activity
{

	private $name;

	private $email;

	private $published;

	private $updated;

	private $commit;

	public function getName()
	{
		return $this->name;
	}

	public function setName($val)
	{
		$this->name = $val;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($val)
	{
		$this->email = $val;
	}

	public function getPublished()
	{
		return $this->published;
	}

	public function setPublished($val = null)
	{
		if ($val) {
			$this->published = $val;
		} else {
			$this->published = new \DateTime("now");
		}
	}

	public function getUpdated()
	{
		return $this->updated;
	}

	public function setUpdated($val = null)
	{
		if ($val) {
			$this->updated = $val;
		} else {
			$this->updated = new \DateTime("now");
		}
	}

	public function getCommit()
	{
		return $this->commit;
	}

	public function setCommit($val)
	{
		$this->commit = $val;
	}
}
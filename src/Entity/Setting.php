<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class Setting
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="string", length=255)
	 */
	private $id;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $value = '1';


	public function setId($val)
	{
		$this->id = $val;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function setValue($val)
	{
		$this->value = $val;
	}

}
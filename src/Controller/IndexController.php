<?php

namespace App\Controller;

use App\Entity\User as User;
use App\Service\BitbucketActivityService;
use App\Service\JiraActivityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{


	public function getBitbucketActivity(BitbucketActivityService $bitbucketservice)
	{
		$response = $bitbucketservice->loadBitbucketActivity();

		return new Response('true');
	}

	public function getJiraActivity(JiraActivityService $jiraservice)
	{
		$jiraservice->loadJiraActivity();

		return new Response('true');
	}


	private function orderUsers($users) {

		$ordered_users = [];

		foreach ($users as $user) {
			if ($user->getActivityBitbucket() > $user->getActivityJira()) {
				$user->setLastActivity($user->getActivityBitbucket());
			}else{
				$user->setLastActivity($user->getActivityJira());
			}
			$ordered_users[] = $user;
		}

		usort($ordered_users, function($first, $second) {
		    return $first->getLastActivity() > $second->getLastActivity();
		});

		return $ordered_users;
	}

	private function filterUsers($users) {

		$filtered_users = [];
		$historic_date = new \Datetime('-2 month');

		foreach ($users as $user) {
			if ($user->getLastActivity() > $historic_date  ) {
				$filtered_users[] = $user;
			}
		}

		return $filtered_users;
	}

	public function highlightEmails($users) {

		$huser = [];

		foreach ($users as $user) {
			$user->isValidEmail = false;
			if (strpos($user->getEmail(), '@u.plus')) {
				$user->isValidEmail = true;
			}
			if (strpos($user->getEmail(), '@usertechnologies.com')) {
				$user->isValidEmail = true;
			}
			$huser[] = $user;
		}

		return $huser;
	}

	public function insertDeadline($users) {

		$huser = [];
		$historic_date = new \Datetime('-1 day');

		foreach ($users as $user) {
			$user->active = false;
			if ($user->getLastActivity() > $historic_date){
				$user->active = true;
			}
			$huser[] = $user;
		}

		return $huser;
	}

	public function fixBitbucketLinks($users) {

		$huser = [];

		foreach ($users as $user) {

			$url = $user->getActivityBitbucketCommit();
			$url = str_replace('https://api.bitbucket.org/2.0/repositories/','https://bitbucket.org/', $url);
			$url = str_replace('/commit/','/commits/', $url);
			$user->setActivityBitbucketCommit($url);
			$huser[] = $user;
		}

		return $huser;
	}

	public function index()
	{

		$users = $this->getDoctrine()
			->getRepository(User::class)
			->OrderedDate();

		$ordered_users = $this->orderUsers($users);
		$filtered_users = $this->filterUsers($ordered_users);
		$highlighted_users = $this->highlightEmails($filtered_users);
		$deadline_users = $this->insertDeadline($highlighted_users);
		$fixlinks_users = $this->fixBitbucketLinks($deadline_users);

		return $this->render('default/index.html.twig', array(
			'users' => $fixlinks_users,
		));

	}


}
